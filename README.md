# Countries App

Countries app is built in Angular where you can get information about countries.

## REST Countries API

REST countries API is used to fetch data in this app.

`https://restcountries.eu`

## Live Demo

[Check here](https://alpertoy.gitlab.io/countries-app/)

