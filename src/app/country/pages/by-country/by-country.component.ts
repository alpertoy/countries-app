import { Country } from './../../interfaces/country.interface';
import { CountryService } from './../../services/country.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-by-country',
  templateUrl: './by-country.component.html',
  styles: [
  ]
})
export class ByCountryComponent implements OnInit {

  term: string = '';
  hasError: boolean = false;
  countries: Country[] = [];

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
  }

  search(term: string) {
    this.hasError = false;
    this.term = term;

    this.countryService.searchCountry(term)
      .subscribe((countries) => {
        this.countries = countries;
      }, (err) => {
        this.hasError = true;
        this.countries = [];
      });
  }

  suggestions(term: string) {
    this.hasError = false;
  }

}
